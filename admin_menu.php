<li class="xn-title">Navigation</li>
                    <li <?php if($this->uri->segment(1) == "meem"){echo "class='active'";}?>>
                        <a href="<?php echo base_url();?>meem/index"><span class="fa fa-desktop"></span> <span class="xn-text">Dashboard</span></a>                        
                    </li>
                    <li <?php if($this->uri->segment(1) =="customer"){echo "class='active'";}?>>
                        <a href="<?php echo base_url();?>customer/index"><span class="fa fa-users"></span> <span class="xn-text">Customer</span></a>                        
                    </li>
                    <li <?php if($this->uri->segment(1) =="driver"){echo "class='active'";}?>>
                        <a href="<?php echo base_url();?>driver/index"><span class="fa fa-cab"></span> <span class="xn-text">Drivers</span></a>                        
                    </li>
                    <li <?php if($this->uri->segment(1) =="trip"){echo "class='active'";}?>>
                        <a href="<?php echo base_url();?>trip/index"><span class="fa fa-road"></span> <span class="xn-text">Trips</span></a>                        
                    </li>  
                    <li <?php if($this->uri->segment(1) =="booking"){echo "class='active'";}?>>
                        <a href="<?php echo base_url();?>booking/index"><span class="fa fa-tasks"></span> <span class="xn-text">Bookings</span></a>                        
                    </li>  
                    <li <?php if($this->uri->segment(1) =="city"){echo "class='active'";}?>>
                        <a href="<?php echo base_url();?>city/index"><span class="fa fa-building-o"></span> <span class="xn-text">City</span></a>                        
                    </li> 
                    <li <?php if($this->uri->segment(1) =="map"){echo "class='active'";}?>>
                        <a href="<?php echo base_url();?>map/index"><span class="fa fa-map-marker"></span> <span class="xn-text">Map</span></a>                        
                    </li>   
                    <li <?php if($this->uri->segment(1) =="report"){echo "class='active'";}?> class="xn-openable">
                        <a href="<?php echo base_url();?>report/index"><span class="fa fa-bar-chart-o"></span> <span class="xn-text">Reports</span></a>
                        <ul>
                            <li><a href="layout-boxed.html">Customer Report</a></li>
                            <li><a href="layout-nav-toggled.html">Driver Report</a></li>
                            <li><a href="layout-nav-top.html">Trips Report</a></li>
                            <li><a href="layout-nav-right.html">Bookings Report</a></li>
                            <li><a href="layout-nav-right.html">Earnings Report</a></li>
                        </ul>
                    </li>                 
                    <li <?php if($this->uri->segment(1) =="menu"){echo "class='active'";}?>>
                        <a href="<?php echo base_url();?>menu/index"><span class="fa fa-files-o"></span> <span class="xn-text">Menu</span></a>                        
                    </li>  
                    <li <?php if($this->uri->segment(1) =="newsletter"){echo "class='active'";}?>>
                        <a href="<?php echo base_url();?>newsletter/index"><span class="fa fa-envelope-o"></span> <span class="xn-text">Newsletter</span></a>                        
                    </li>  
                    <li  <?php if($this->uri->segment(1) =="settings"){echo "class='active'";}?> class="xn-openable">
                        <a href="<?php echo base_url();?>auth"><span class="fa fa-cogs"></span> <span class="xn-text">Settings</span></a>
                        <ul>
                            <li><a href="<?php echo base_url();?>settings/profile"><span class="xn-text">Profile</span></a></li>
                            <li><a href="<?php echo base_url();?>settings/change_password"><span class="xn-text">Change Password</span></a></li>
                        </ul>
                    </li>              
                    <li  <?php if($this->uri->segment(1) =="admin-user"){echo "class='active'";}?> class="xn-openable">
                        <a href="<?php echo base_url();?>auth"><span class="fa fa-user"></span> <span class="xn-text">Admin Control</span></a>
                        <ul>
                            <li><a href="<?php echo base_url();?>auth/index"><span class="xn-text">User</span></a></li>
                            <li><a href="<?php echo base_url();?>auth/create_group"><span class="xn-text">Group</span></a></li>
                            
                        </ul>
                    </li>      
                    <li <?php if($this->uri->segment(1) =="promocode"){echo "class='active'";}?>>
                        <a href="<?php echo base_url();?>promocode/index"><span class="fa fa-bell-o"></span> <span class="xn-text">Promocode</span></a>                        
                    </li>  
                    <li <?php if($this->uri->segment(1) =="wallet"){echo "class='active'";}?>>
                        <a href="<?php echo base_url();?>wallet/index"><span class="fa fa-money"></span> <span class="xn-text">Wallet</span></a>                        
                    </li> 