
        <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
                    <div class="mb-content">
                        <p>Are you sure you want to log out?</p>                    
                        <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
                    </div>
                    <div class="mb-footer">
                        <div class="pull-right">
                            <a href="<?php echo base_url();?>auth/logout" class="btn btn-success btn-lg">Yes</a>
                            <button class="btn btn-default btn-lg mb-control-close">No</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MESSAGE BOX-->


        <!-- START PRELOADS -->
        <audio id="audio-alert" src="<?php echo $this->config->item('backend_audio_url');?>alert.mp3" preload="auto"></audio>
        <audio id="audio-fail" src="<?php echo $this->config->item('backend_audio_url');?>fail.mp3" preload="auto"></audio>
        <!-- END PRELOADS -->                  
        


    <!-- START SCRIPTS -->
        <!-- START PLUGINS -->
        
        <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/bootstrap/bootstrap.min.js"></script>        
        <!-- END PLUGINS -->

        <!-- START THIS PAGE PLUGINS-->        
        <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/icheck/icheck.min.js'></script>        
        <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js"></script>
        <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/scrolltotop/scrolltopcontrol.js"></script>
        <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/bootstrap/bootstrap-datepicker.js'></script>
    <!-- START Dashbord PAGE PLUGINS-->
        <?php if($this->uri->segment(1) == "meem" ){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/morris/raphael-min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/morris/morris.min.js"></script>       
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/rickshaw/d3.v3.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/rickshaw/rickshaw.min.js"></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>                
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>demo_dashboard.js"></script>               
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/owl/owl.carousel.min.js"></script>                 
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/moment.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/daterangepicker/daterangepicker.js"></script>
        <?php } ?>
    <!-- End Dashbord PAGE PLUGINS-->

    <!-- START Customer PAGE PLUGINS-->
        <?php if($this->uri->segment(1) == "customer" && $this->uri->segment(2) == "index"  ){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/jquery.validationEngine.js'></script>        
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jquery-validation/jquery.validate.js'></script>                
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/maskedinput/jquery.maskedinput.min.js'></script>
            
        <?php }?>
        <?php if($this->uri->segment(1) == "customer" && $this->uri->segment(2) == "create"  ){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/jquery.validationEngine.js'></script>        
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jquery-validation/jquery.validate.js'></script>                
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/maskedinput/jquery.maskedinput.min.js'></script>
            <script type="text/javascript">
                var jvalidate = $("#jvalidate").validate({
                    ignore: [],
                    rules: {                                            
                            'email': {
                                    required: true,
                                    email: true
                            },
                            'cust_name': {
                                    required: true
                            },
                            'cust_mobile_no' : {
                                    required: true,
                                    minlength: 10,
                                    maxlength: 10
                            }
                        }                                        
                    });                                    

            </script>
        <?php }?>
        <?php if($this->uri->segment(1) == "customer" && $this->uri->segment(2) == "edit"  ){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/jquery.validationEngine.js'></script>        
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jquery-validation/jquery.validate.js'></script>                
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/maskedinput/jquery.maskedinput.min.js'></script>
            
        <?php }?>
     <!-- END CUSTOMER PAGE PLUGINS-->        
     <!-- START DRIVER PAGE PLUGINS-->
        <?php if($this->uri->segment(1) == "driver" && $this->uri->segment(2) == "edit" ){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/jquery.validationEngine.js'></script>        
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jquery-validation/jquery.validate.js'></script>                
        <?php }?>
        <?php if($this->uri->segment(1) == "driver" && $this->uri->segment(2) == "create" ){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/jquery.validationEngine.js'></script>        
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jquery-validation/jquery.validate.js'></script>                
            
            <script type="text/javascript">
                var jvalidate = $("#jvalidate").validate({
                    ignore: [],
                    rules: {                                            
                            'dri_email': {
                                    required: true,
                                    email: true
                            },
                            'dri_name': {
                                    required: true
                            },
                            'dri_mo' : {
                                    required: true,
                                   minlength: 10,
                                    maxlength: 10
                            },
                            'dri_aadhar' : {
                                    required: true,
                                    minlength: 12,
                                    maxlength: 12
                            },
                            'dri_lic' : {
                                    required: true,
                                    minlength: 15,
                                    maxlength: 15
                            },
                            'pan_no' : {
                                    required: true,
                                    minlength: 10,
                                    maxlength: 10
                            }
											
                          }                                        
                });                                    

            </script>
        <?php }?>
        <?php if($this->uri->segment(1) == "driver" && $this->uri->segment(2) == "index" ){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/jquery.validationEngine.js'></script>        
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jquery-validation/jquery.validate.js'></script>                
            <script type="text/javascript">
        
                var address = 
                        $('.driver_status').on('change', function() {
                            var whole_data = this.value;
                            var data = whole_data.split(",");
                            //console.log();
                            //console.log(data[1]);return false;

                            var request = $.ajax({
                              url: "<?php echo base_url();?>driver/change_status",
                              type: "POST",
                              data: {driver_id : data[1], driver_status : data[0]}
                            //  dataType: "json"
                            });
                            
                             request.done( function(msg) {
                             if(msg){
                                location.reload();
                            }
                            });

                            request.fail(function(jqXHR, textStatus) {
                              alert( "Request failed: " + textStatus );
                            });
                        })

            </script>
        <?php }?>
		<?php if($this->uri->segment(1) == "driver" && $this->uri->segment(2) == "email_availability" ){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/jquery.validationEngine.js'></script>        
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jquery-validation/jquery.validate.js'></script>                
        	<script type="text/javascript">
                $("#dri_email").blur(function(){
                    var emailId = $('#dri_email').val();
                    $.ajax({
                        url: "<?php echo base_url()?>driver/email_availability", 
                        method:"POST",
                        data:{
                                'email': emailId
                            },
                        success: function(result){
                        if(result == 1){
                            $('#email_msg').html('Email Id already exist');
                            $('#dri_email').val('');
                            return false;
                        }
                        else{
                            $('#email_msg').html('Email available');
                        }
                        
                        }});
                    });
            </script>  

        <?php }?>

    <!-- END DRIVER PAGE PLUGINS--> 
    <!-- START TRIP PAGE PLUGINS-->
        <?php if($this->uri->segment(1) == "trip" && $this->uri->segment(2) == "index" ){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/jquery.validationEngine.js'></script>        
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jquery-validation/jquery.validate.js'></script>                
        <?php }?>
        <?php if($this->uri->segment(1) == "trip" && $this->uri->segment(2) == "edit" ){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/jquery.validationEngine.js'></script>        
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jquery-validation/jquery.validate.js'></script>                
            <script type="text/javascript">
                var jvalidate = $("#jvalidate").validate({
                    ignore: [],
                    rules: {                                            
                        'trip_type': {
                                required: true  
                        },
                        'trip_source': {
                                required: true
                        },
                        'trip_dest' : {
                                required: true
                        },
                        'trip_bookingr' : {
                                required: true
                        },
                        'trip_estimate' : {
                                required: true
                        },
                        'trip_car_type' : {
                                required: true         
                        }
                    }                                        
                });                                    

            </script>
        <?php }?>   

    <!-- END TRIP PAGE PLUGINS--> 

    <!-- START BOOKING PAGE PLUGINS-->
        <?php if($this->uri->segment(1) == "booking" && $this->uri->segment(2) == "index"){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script> 
        <?php }?>    
        <?php if($this->uri->segment(1) == "booking" && $this->uri->segment(2) == "edit"){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script> 
        <?php }?>    

    <!-- END BOOKING PAGE PLUGINS--> 
    <!-- START CITY PAGE PLUGINS-->
         <?php if($this->uri->segment(1) == "city" && $this->uri->segment(2) == "create" ){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/jquery.validationEngine.js'></script>        
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jquery-validation/jquery.validate.js'></script>                
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/maskedinput/jquery.maskedinput.min.js'></script>
            <script type="text/javascript">
                    var jvalidate = $("#jvalidate").validate({
                        ignore: [],
                        rules: {                                            
                                'city_name': {
                                        required: true
                                }
                            }                                        
                        });                                    

            </script>
        <?php }?>
            <?php if($this->uri->segment(1) == "city" && $this->uri->segment(2) == "index" ){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/jquery.validationEngine.js'></script>        
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jquery-validation/jquery.validate.js'></script>                
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/maskedinput/jquery.maskedinput.min.js'></script>
        <?php }?>
        <?php if($this->uri->segment(1) == "city" && $this->uri->segment(2) == "edit" ){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/jquery.validationEngine.js'></script>        
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jquery-validation/jquery.validate.js'></script>                
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/maskedinput/jquery.maskedinput.min.js'></script>
        <?php }?>
    <!-- END CITY PAGE PLUGINS--> 
    <!-- START MAP PAGE PLUGINS-->
        <?php if($this->uri->segment(1) == "map"){?>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtUXWuwilf-NJ_29cPQbVdH3aev8oYAjk&v=3.exp&sensor=false&libraries=places"></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jvectormap/jquery-jvectormap-europe-mill-en.js'></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jvectormap/jquery-jvectormap-us-aea-en.js'></script>
            <script type="text/javascript">
                if($("#google_ptm_map").length > 0){
                
                   //Time between marker refreshes
                    var INTERVAL = 2000;
                    
                        //Used to remember markers
                    var markerStore = {};
                    
                    var myLatlng = new google.maps.LatLng(18.520430, 73.856744);
                    var myOptions = {
                        zoom: 10,
                        center: myLatlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                }
                    var map = new google.maps.Map(document.getElementById("google_ptm_map"), myOptions);

                    getMarkers();   

                function getMarkers() {
                    $.get('get_all_driver_location', {}, function(res,resp) {
                        console.log(res);
                        for(var i=0, len=res.length; i<len; i++) {

                            //Do we have this marker already?
                            if(markerStore.hasOwnProperty(res[i].driver_id)) {
                                markerStore[res[i].driver_id].setPosition(new google.maps.LatLng(res[i].latitude,res[i].longitude));
                            } else {
                                var marker = new google.maps.Marker({
                                    position: new google.maps.LatLng(res[i].latitude,res[i].longitude),
                                    title:res[i].driver_name,
                                    map:map
                                }); 
                                markerStore[res[i].driver_id] = marker;
 
                                var content = "Name: " + res[i].driver_name + '<br/>' + "Mobile Number: " + res[i].driver_mobile_no + '<br />' + "Last Update At : " + res[i].updated_at    

                                var infowindow = new google.maps.InfoWindow()

                                google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
                                return function() {
                                infowindow.setContent(content);
                                infowindow.open(map,marker);
                                };
                                })(marker,content,infowindow));
                            }
                        }
                        window.setTimeout(getMarkers,INTERVAL);
                    }, "json");
                }
            }
        </script>
        <?php }?>
        <!-- END TEMPLATE -->    
    <!-- END SCRIPTS -->



    <!-- END MAP PAGE PLUGINS-->
    <!-- START VIEW HISTORY PAGE PLUGINS-->
       <?php if($this->uri->segment(2) == "view_history"  ){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/jquery.validationEngine.js'></script>        
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jquery-validation/jquery.validate.js'></script>
			<script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/maskedinput/jquery.maskedinput.min.js'></script>
      <?php }?>    

    <!-- END VIEW HISTORY PAGE PLUGINS-->
    <!-- Start Customer report PAGE PLUGINS-->
        <?php if($this->uri->segment(2) == "customer_report"){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script> 
            
        <?php }?>        

       <?php if($this->uri->segment(2) == "customer_details"){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/morris/raphael-min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/morris/morris.min.js"></script>       
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/rickshaw/d3.v3.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/owl/owl.carousel.min.js"></script>                 
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>demo_dashboard.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/rickshaw/rickshaw.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script> 
        <?php }?>    
    <!-- END Customer report PLUGINS-->

     <!-- Start Driver report PAGE PLUGINS-->
        <?php if($this->uri->segment(2) == "driver_report"){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script> 
            
        <?php }?>        

       <?php if($this->uri->segment(2) == "driver_details"){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/morris/raphael-min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/morris/morris.min.js"></script>       
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/rickshaw/d3.v3.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/owl/owl.carousel.min.js"></script>                 
            
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/rickshaw/rickshaw.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script> 
        <?php }?>    
    <!-- END Driver report PLUGINS-->
     
    <!-- Start Trip report PAGE PLUGINS-->
        <?php if($this->uri->segment(2) == "trips_report"){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script> 
            
        <?php }?>        
    <!-- END Trip report PLUGINS-->

         <!-- Start Vendor Reports PAGE PLUGINS-->
        <?php if($this->uri->segment(2) == "vendor_report"){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script> 
            
        <?php }?>        

       <?php if($this->uri->segment(2) == "vendor_dashboard"){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/morris/raphael-min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/morris/morris.min.js"></script>       
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/rickshaw/d3.v3.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/owl/owl.carousel.min.js"></script>                 
            
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/rickshaw/rickshaw.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script> 
        <?php }?>    
    <!-- END Vendor Reports PLUGINS-->
 
    <!-- START AUTH  PAGE PLUGINS-->
        <?php if($this->uri->segment(1) == "auth"){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script> 
            
        <?php }?>

    <!-- END INDEX  PAGE PLUGINS-->

 


 
    <!-- START NEWSLETTWER  PAGE PLUGINS-->
        <?php if($this->uri->segment(1) == "newsletter" && $this->uri->segment(2) == "create"){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script> 
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/jquery.validationEngine.js'></script>        
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jquery-validation/jquery.validate.js'></script>                
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/maskedinput/jquery.maskedinput.min.js'></script>
            <script type="text/javascript">
                var jvalidate = $("#jvalidate").validate({
                ignore: [],
                rules: {                                            
                        'newsletter_content': {
                                required: true
                            }
                    }                                        
                });                                    

            </script>
        <?php }?>

    <!-- END NEWSLETTWER PAGE PLUGINS-->

    <!-- START PROMOCODE  PAGE PLUGINS-->
        <?php if($this->uri->segment(1) == "promocode"){?>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/languages/jquery.validationEngine-en.js'></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/validationengine/jquery.validationEngine.js'></script>        

            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jquery-validation/jquery.validate.js'></script>                

            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/maskedinput/jquery.maskedinput.min.js'></script>

            <script type="text/javascript">
                var jvalidate = $("#jvalidate").validate({
                    ignore: [],
                    rules: {                                            
                            'promo': {
                                    required: true
                                    
                            },
                            'promo_dis': {
                                    required: true,
                                    minlength: 02,
                                    maxlength: 02
                            }
                        }                                        
                    });                                    

            </script>
        <?php }?>
    
    <!-- END PROMOCODE  PAGE PLUGINS-->
    
    <!-- START VENDOR  PAGE PLUGINS--> 
        <?php if($this->uri->segment(1) == "vendor"){ if($this->uri->segment(2) == "request_driver" || $this->uri->segment(2) == "edit_request_driver"){?> 
				<script type="text/javascript">
				   $(function  () {
				   $("#ddlTrip").change(function () {
					if ($(this).val() == "Delivery") {
						$("#dvAdv").show();
					 } else {
						$("#dvAdv").hide();
					 }
					});
				  });
				</script>
				<script type="text/javascript">
                    function validate() {
                      if (document.getElementById("advisor_name").value == "") {
                       alert("User name may not be blank");
                     }  else if (document.getElementById("advisor_number").value == "") {
                       alert("Password may not be blank.");
                 }
                }                             

            </script>
    	     <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/bootstrap/bootstrap-select.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/bootstrap/bootstrap-datepicker.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/bootstrap/bootstrap-timepicker.min.js"></script>
        <?php }else if($this->uri->segment(2) == "todays_trip"){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/bootstrap/bootstrap-select.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/bootstrap/bootstrap-datepicker.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/bootstrap/bootstrap-timepicker.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script>
        <?php } else if($this->uri->segment(2) == "active_trip"){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/bootstrap/bootstrap-select.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/bootstrap/bootstrap-datepicker.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/bootstrap/bootstrap-timepicker.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script>
        <?php } else if($this->uri->segment(2) == "change_password"){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/bootstrap/bootstrap-select.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/bootstrap/bootstrap-datepicker.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/bootstrap/bootstrap-timepicker.min.js"></script>
        <?php } elseif($this->uri->segment(2) == "view_history" || $this->uri->segment(2) == "get_trips_by_date" || $this->uri->segment(2) == "view_jobcards"){?>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/datatables/jquery.dataTables.min.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/tableExport.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jquery.base64.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/html2canvas.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/sprintf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/jspdf.js"></script>
            <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins/tableexport/jspdf/libs/base64.js"></script>
        <?php }?>

            <script type="text/javascript">
            
                    var address = 
                    $('#trip_type').on('change', function() {
                        var trip_type = this.value;

                    var request = $.ajax({
                      url: "<?php echo base_url();?>vendor/get_vendor_address",
                      type: "POST",
                      data: {id : <?php echo $this->session->userdata('user_id');?>},
                      dataType: "json"
                    });

                     request.done(function(msg) {
                    var data = jQuery.parseJSON(JSON.stringify(msg));
                      if(trip_type == "Collection"){
                        
                        //$("#to_address").prop('readonly', true);
                        $('#to_address').val(data['company']);
                        
                        //$("#from_address").prop('readonly', false);
                        $('#from_address').val('');

                        $(document).on ("blur", "#from_address", function() {setadd($('#from_address').val())});

                      }else{
                        
                        //$("#from_address").prop('readonly', true);
                        $('#from_address').val(data['company']); 

                        //$("#to_address").prop('readonly', false);
                        $('#to_address').val(''); 

                        //$("#to_address").bind("blur", $('#cust_address').val('bbbbb'));   
                        $(document).on ("blur", "#to_address", function() {setadd($('#to_address').val())});

                      }

                    });

                    request.fail(function(jqXHR, textStatus) {
                      alert( "Request failed: " + textStatus );
                    });
                })

            </script>
            <script type="text/javascript">
                function setadd(currentVal){
                    $('#cust_address').val(currentVal);
                }
            </script>
            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDtUXWuwilf-NJ_29cPQbVdH3aev8oYAjk&v=3.exp&sensor=false&libraries=places"></script>

            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jvectormap/jquery-jvectormap-world-mill-en.js'></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jvectormap/jquery-jvectormap-europe-mill-en.js'></script>
            <script type='text/javascript' src='<?php echo $this->config->item('backend_js_url');?>plugins/jvectormap/jquery-jvectormap-us-aea-en.js'></script>

            <script type="text/javascript">
                if($("#google_ptm_map").length > 0){

                    //Time between marker refreshes
                    var INTERVAL = 2000;
                    
                    //Used to remember markers
                    var markerStore = {};
                    
                    var myLatlng = new google.maps.LatLng(18.520430, 73.856744);
                    var myOptions = {
                        zoom: 10,
                        center: myLatlng,
                        mapTypeId: google.maps.MapTypeId.ROADMAP,
                    }
                    var map = new google.maps.Map(document.getElementById("google_ptm_map"), myOptions);

                    getMarkers();   

                    function getMarkers() {
                        $.get('get_driver_location', {}, function(res,resp) {
                            //console.dir(res);
                            for(var i=0, len=res.length; i<len; i++) {

                                //Do we have this marker already?
                                if(markerStore.hasOwnProperty(res[i].driver_id)) {
                                    markerStore[res[i].driver_id].setPosition(new google.maps.LatLng(res[i].latitude,res[i].longitude));
                                } else {
                                    var marker = new google.maps.Marker({
                                        position: new google.maps.LatLng(res[i].latitude,res[i].longitude),
                                        title:res[i].driver_name,
                                        map:map
                                    }); 
                                    markerStore[res[i].driver_id] = marker;

                                    var content = "Name: " + res[i].driver_name + '<br/>' + "Mobile Number: " + res[i].driver_mobile_no +  '</br>' + " From: " + res[i].trip_from_locn + '<br/>' + " To: " + res[i].trip_to_locn + '<br/>' + "Date-Time: " + res[i].trip_booking_date_time    

                                    var infowindow = new google.maps.InfoWindow()

                                    google.maps.event.addListener(marker,'click', (function(marker,content,infowindow){ 
                                    return function() {
                                    infowindow.setContent(content);
                                    infowindow.open(map,marker);
                                    };
                                    })(marker,content,infowindow));
                                }
                            }
                            window.setTimeout(getMarkers,INTERVAL);
                        }, "json");
                    }
                }
            </script>
        <?php }?>

    <!-- END VENDOR  PAGE PLUGINS--> 
    <!-- START TEMPLATE -->
        <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>settings.js"></script>
         
        <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>plugins.js"></script>        
        <script type="text/javascript" src="<?php echo $this->config->item('backend_js_url');?>actions.js"></script>

    </body>
</html>